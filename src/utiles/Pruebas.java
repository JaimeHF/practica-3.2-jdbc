package utiles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelos.Equipo;
import modelos.Jugador;

public class Pruebas {

    public static void main(String[] args) {

        String cadena_conexion = "jdbc:mysql://localhost:3307/liga";
        String usuario = "root";
        String contrasena = "1234";

        try {
            
            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();
            
            PreparedStatement stm = conn.prepareStatement("select * from jugadores");
            ResultSet rs  = stm.executeQuery();

            System.out.println("Tabla jugador");
            while (rs.next()) {
                int id = rs.getInt("jugador_id");
                String nombre = rs.getString("nombre_jugador");
                String posicion = rs.getString("posicion");
                String nacionalidad = rs.getString("nacionalidad");
                int dorsal = rs.getInt("dorsal");
                int equipo = rs.getInt("equipos_id");
                Jugador j1 = new Jugador(id, nombre, posicion,nacionalidad,dorsal,equipo);
                
                System.out.println(j1);
            }
            stm = conn.prepareStatement("select * from equipos");
            rs  = stm.executeQuery();
            
            System.out.println("\nTabla equipo");
            while (rs.next()) {
                int id = rs.getInt("equipos_id");
                String nombre = rs.getString("nombre_equipo");
                int puntuacion = rs.getInt("puntuacion");
                String ciudad = rs.getString("ciudad");
                String nombreEstadio = rs.getString("nombre_estadio");
                int liga = rs.getInt("liga_id");
                Equipo e1 = new Equipo(id, nombre,puntuacion,ciudad,nombreEstadio,liga);
                
                System.out.println(e1);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }

}
