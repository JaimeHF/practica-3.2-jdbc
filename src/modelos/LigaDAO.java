package modelos;

import java.sql.ResultSet;

public interface LigaDAO {
        // CRUD
    public ResultSet selectAllLiga();
    public Liga insertLiga(int liga_id, String nombre_liga, String pais_liga);
    public boolean deleteLiga(int liga_id);
    public Liga updateLiga(int liga_id, String nombre_liga, String pais_liga);
}
