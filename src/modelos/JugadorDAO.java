package modelos;

import java.sql.ResultSet;

public interface JugadorDAO {
            // CRUD
    public ResultSet selectAllJugador();
    public Jugador insertJugador(int jugador_id, String nombre_jugador, String posicion, String nacionalidad, int dorsal, int equipo_id);
    public boolean deleteJugador(int jugador_id);
    public Jugador updateJugador(int jugador_id, String nombre_jugador, String posicion, String nacionalidad, int dorsal, int equipo_id);
    
}
