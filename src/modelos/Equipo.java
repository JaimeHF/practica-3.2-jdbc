package modelos;

public class Equipo {
    private int equipo_id;
    private String nombre_equipo;
    private int puntos;
    private String ciudad;
    private String nombre_estadio;
    private int liga_id;

    public Equipo(int equipo_id, String nombre_equipo, int puntos, String ciudad, String nombre_estadio, int liga_id) {
        this.equipo_id = equipo_id;
        this.nombre_equipo = nombre_equipo;
        this.puntos = puntos;
        this.ciudad = ciudad;
        this.nombre_estadio = nombre_estadio;
        this.liga_id = liga_id;
    }
    
    
    

    public int getEquipo_id() {
        return equipo_id;
    }

    public String getNombre_equipo() {
        return nombre_equipo;
    }

    public int getPuntos() {
        return puntos;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getNombre_estadio() {
        return nombre_estadio;
    }

    public int getLiga_id() {
        return liga_id;
    }

    public void setEquipo_id(int equipo_id) {
        this.equipo_id = equipo_id;
    }

    public void setNombre_equipo(String nombre_equipo) {
        this.nombre_equipo = nombre_equipo;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setNombre_estadio(String nombre_estadio) {
        this.nombre_estadio = nombre_estadio;
    }

    public void setLiga_id(int liga_id) {
        this.liga_id = liga_id;
    }

    @Override
    public String toString() {
        return "Equipo{" + "equipo_id=" + equipo_id + ", nombre_equipo=" + nombre_equipo + ", puntos=" + puntos + ", ciudad=" + ciudad + ", nombre_estadio=" + nombre_estadio + ", liga_id=" + liga_id + '}';
    }
    
    
    
}
