package modelos;

public class Jugador {
    private int jugador_id;
    private String nombre_jugador;
    private String posicion;
    private String nacionalidad;
    private int dorsal;
    private int equipo_id;

    public Jugador(int jugador_id, String nombre_jugador, String posicion, String nacionalidad, int dorsal, int equipo_id) {
        this.jugador_id = jugador_id;
        this.nombre_jugador = nombre_jugador;
        this.posicion = posicion;
        this.nacionalidad = nacionalidad;
        this.dorsal = dorsal;
        this.equipo_id = equipo_id;
    }
   

    public int getJugador_id() {
        return jugador_id;
    }

    public String getNombre_jugador() {
        return nombre_jugador;
    }

    public String getPosicion() {
        return posicion;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public int getDorsal() {
        return dorsal;
    }

    public int getEquipo_id() {
        return equipo_id;
    }

    public void setJugador_id(int jugador_id) {
        this.jugador_id = jugador_id;
    }

    public void setNombre_jugador(String nombre_jugador) {
        this.nombre_jugador = nombre_jugador;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public void setEquipo_id(int equipo_id) {
        this.equipo_id = equipo_id;
    }

    @Override
    public String toString() {
        return "Jugador{" + "jugador_id=" + jugador_id + ", nombre_jugador=" + nombre_jugador + ", posicion=" + posicion + ", nacionalidad=" + nacionalidad + ", dorsal=" + dorsal + ", equipo_id=" + equipo_id + '}';
    }
    
    
    
    
    
}
