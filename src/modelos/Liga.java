package modelos;

public class Liga {
    private int liga_id;
    private String nombre_liga;
    private String pais_liga;

    public Liga(int liga_id, String nombre_liga, String pais_liga) {
        this.liga_id = liga_id;
        this.nombre_liga = nombre_liga;
        this.pais_liga = pais_liga;
    }
    

    public int getLiga_id() {
        return liga_id;
    }

    public String getNombre_liga() {
        return nombre_liga;
    }

    public String getPais_liga() {
        return pais_liga;
    }

    public void setLiga_id(int liga_id) {
        this.liga_id = liga_id;
    }

    public void setNombre_liga(String nombre_liga) {
        this.nombre_liga = nombre_liga;
    }

    public void setPais_liga(String pais_liga) {
        this.pais_liga = pais_liga;
    }

    @Override
    public String toString() {
        return "Liga{" + "liga_id=" + liga_id + ", nombre_liga=" + nombre_liga + ", pais_liga=" + pais_liga + '}';
    }
    
    
    
}
