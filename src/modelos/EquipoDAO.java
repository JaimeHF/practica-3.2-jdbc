package modelos;

import java.sql.ResultSet;


public interface EquipoDAO {
    public ResultSet selectAllEquipo();
    public Equipo insertEquipo(int equipo_id, String nombre_equipo, int puntos, String ciudad, String nombre_estadio, int liga_id);
    public boolean deleteEquipo(int equipo_id);
    public Equipo updateEquipo(int equipo_id, String nombre_equipo, int puntos, String ciudad, String nombre_estadio, int liga_id);
    
}
